Apache configuration
====================
Make sure your Apache virtual host (the same host used in below command, in this case localhost ) is configured with the AllowOverride option so that the .htaccess rewrite rules can be used:

AllowOverride All

Project Configuration
=====================
Edit PHPOnlineMerchantUtil/resources/properties.ini file and put secret merchant Key in the file

merchantKey = XXXXX-ABC-5678-ZZZZ-ABCDE

PHP built-in server
===================
Run the following command in terminal to start localhost web server, assuming ./PHPOnlineMerchantUtil/ is public-accessible directory with index.php file:

php -S localhost:8080 -t ./PHPOnlineMerchantUtil/


HTTP Request
============
Api endpoint :  http://localhost:8080/api/vi/coi/checksum

HTTP method  :  POST (GET not supported)

Headers      :  Content-Type: application/json (other content types not supported)

Request body :  JSON object(JSON representation of request). All JSON values MUST BE Strings.

Example,

POST http://localhost:8080/api/vi/coi/checksum
Content-Type: application/json
{
	"merchantTxnId": "iTEST1_***sdsds%1233",
	"amount": "200.65",
	"currency": "INR",
	"furl": "http://52.71.29.147:8080//failure.html?ji=ered&sdee=3423",
	"surl": "http://52.71.29.147:8080//success.html?hi=ho&abc=abr",
	"productInfo": "auth",
	"email": "test2323))@test.com",
	"mobile": "9090909090",
	"customerName": "tes tCustomer",
	"blockPaymentType": null,
	"pg": "",
	"dropCategory": null,
	"customNote": "please make it fast...; !",
	"noteCategory": null,
	"channel": "WEB",
	"metadata": null,
	"os": "ubuntu-14.04",
	"version": null,
	"merchantId": "yiEjM5BfwzD4Ld",
	"checksum": null
}

HTTP Response
==============
Response Status : 200 OK           (for valid cases)
				  400 Bad Request  (for invalid request)

Headers         : Content-Type:  application/json

Response Body   : JSON request with checksum filled in "checksum" field  (for valid cases)
				  {"ERROR":"Invalid json request"}   (for invalid request)
				  
Example,				  		  
				  	
200 OK
Host:  localhost:8080
Connection:  close
X-Powered-By:  PHP/5.5.9-1ubuntu4.14
Content-Type:  application/json
Content-Length:  595

{"merchantTxnId":"iTEST1_***sdsds%1233","amount":"200.65","currency":"INR","furl":"http://52.71.29.147:8080//failure.html?ji=ered&sdee=3423","surl":"http://52.71.29.147:8080//success.html?hi=ho&abc=abr","productInfo":"auth","email":"test2323))@test.com","mobile":"9090909090","customerName":"tes tCustomer","blockPaymentType":null,"pg":"","dropCategory":null,"customNote":"please make it fast...; !","noteCategory":null,"channel":"WEB","metadata":null,"os":"ubuntu-14.04","version":null,"merchantId":"yiEjM5BfwzD4Ld","checksum":"5fc487b4f95ec1a6ff9b0cdc939aa0112cb0b78b56fbac77ac23943d150a7c6f"}
