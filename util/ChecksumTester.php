<?php
require_once __DIR__ . '/../request/MerchantPaymentRequest.class.php';
require_once __DIR__ . '/../request/GetTransactionStatusRequest.class.php';
require_once __DIR__ . '/../response/RefundResponse.class.php';
require_once __DIR__ . '/ChecksumUtil.class.php';
require_once __DIR__ . '/../response/GetAllTransactionsResponse.class.php';
require_once __DIR__ . '/../common/TransactionDTO.class.php';

$req = new MerchantPaymentRequest ();

$req->merchantTxnId = "iTEST1_***sdsds%1233";
$req->amount = "200.65";
$req->currency = "INR";
$req->furl = "http://52.71.29.147:8080//failure.html?ji=ered&sdee=3423";
$req->surl = "http://52.71.29.147:8080//success.html?hi=ho&abc=abr";
$req->productInfo = "auth";
$req->email = "test2323))@test.com";
$req->mobile = "9090909090";
$req->customerName = "tes tCustomer";
$req->pg = "";
$req->channel = "WEB";
$req->merchantId = "yiEjM5BfwzD4Ld";
$req->os = "ubuntu-14.04";
$req->customNote = "please make it fast...; !";
$req->version = NULL;

$txReq = new GetTransactionStatusRequest ();
$txReq->merchantId = "mer__*&^%$#@!";
$txReq->merchantTxnId = "h67799*&&sds";
$txReq->txnType = "CUSTOMER_PAyment s";

$refundResponse = new RefundResponse ();
$refundResponse->merchantTxnId = "h67799*&&sds";
$refundResponse->refundedAmount = "1350.50";
$refundResponse->refundMerchantTxnId = "mer__*&^%$#@!";
$refundResponse->refundTxnId = "mer__*&^%$#@!sdsds___";
$refundResponse->status = "SUCCESS-400";

$getAllTransactionsResponse = new GetAllTransactionsResponse ();

$transactionDTO1 = new TransactionDTO ();
$transactionDTO1->amount = "200.00";
$transactionDTO1->merchantTxnId = "mer__*&^%$#@!";
$transactionDTO1->mode = "PG";
$transactionDTO1->status = "SUCCESS - 400";
$transactionDTO1->txnId = "sjdhjsdhu32 337&&^^%$";

$transactionDTO2 = new TransactionDTO ();
$transactionDTO2->amount = "300.00";
$transactionDTO2->merchantTxnId = "mer__*&^%$#@!";
$transactionDTO2->mode = "WALLET";
$transactionDTO2->status = "SUCCESS - 400";
$transactionDTO2->txnId = "ajdhjsdhu32 337&&^^%$";

$list = array ();
$list [] = $transactionDTO1;
$list [] = $transactionDTO2;

$getAllTransactionsResponse->transactionStatus = $list;

$checksum = ChecksumUtil::generateChecksumForJson ( json_decode ( json_encode ( $req ), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ), "624f1d3e-3486-41b4-9590-3b638c7f3c53" );
var_export ( $checksum );