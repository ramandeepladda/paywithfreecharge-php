<?php
require_once __DIR__ . '/AbstractRequest.class.php';
class MerchantPaymentRequest extends AbstractRequest {
	var $merchantTxnId;
	var $amount;
	var $currency;
	var $furl;
	var $surl;
	var $productInfo;
	var $email;
	var $mobile;
	var $customerName;
	var $blockPaymentType;
	var $pg;
	var $dropCategory;
	var $customNote;
	var $noteCategory;
	var $channel;
	var $metadata;
	var $os;
	var $version;
}