<?php
require_once __DIR__ . '/AbstractRequest.class.php';
class GetAllTransactionsRequest extends AbstractRequest {
	var $startDate;
	var $endDate;
	var $pageNumber;
	var $pageSize;
}