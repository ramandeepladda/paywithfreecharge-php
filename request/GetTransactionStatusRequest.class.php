<?php
require_once __DIR__ . '/AbstractRequest.class.php';
class GetTransactionStatusRequest extends AbstractRequest {
	var $merchantTxnId;
	var $txnId;
	var $txnType;
}