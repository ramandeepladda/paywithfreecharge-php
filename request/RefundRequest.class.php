<?php
require_once __DIR__ . '/AbstractRequest.class.php';
class RefundRequest extends AbstractRequest {
	var $refundTxnId;
	var $txnId;
	var $merchantTxnId;
	var $refundAmount;
	var $comments;
	
}