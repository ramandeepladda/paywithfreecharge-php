<?php
class CommonConstants {
	const ALGO = "sha256";
	const ENCODING = "UTF-8";
	const PAYMENT_GATEWAY = "Freecharge";
	const SERVER_TYPE = "Mysql";
}
