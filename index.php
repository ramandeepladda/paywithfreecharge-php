<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/util/ChecksumUtil.class.php';
require_once __DIR__ . '/common/ExceptionCodes.class.php';
require_once __DIR__ . '/common/ExceptionResponse.class.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Instantiate a Slim application
$app = new \Slim\App ();

// Define a HTTP GET route
$app->post ( '/api/vi/coi/checksum', function (Request $request, Response $response) {
	
	return returnRequestWithChecksum ( $request, $response );
} );

$app->get ( '/api/vi/coi/logintoken', function (Request $request, Response $response) {

        return encrypt ( $request, $response );
} );

// Run the Slim application
$app->run ();

function encrypt (Request $request, Response $response) {
    $accessToken = $_GET ["accessToken"];
    $ini_array = parse_ini_file ( __DIR__ . "/resources/properties.ini" );
    $loginToken = generateLoginToken ( $accessToken, $ini_array ['merchantKey'] );
    $responseWithHeader = $response->withHeader ( 'Content-type', 'application/json' );
    $body = $responseWithHeader->getBody ();
    $body->write ( "{\"loginToken\":\"$loginToken\"" );
    return $responseWithHeader;
}

function generateLoginToken ($accessToken, $secretKey) {
    $plaintext = pkcs5_pad($accessToken, 16);
    $key = substr($secretKey, 0, 16);
    return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_ECB));
}

function pkcs5_pad ($text, $blocksize)
{
    $pad = $blocksize - (strlen($text) % $blocksize);
    return $text . str_repeat(chr($pad), $pad);
}

// Callback function to generate checksum
function returnRequestWithChecksum(Request $request, Response $response) {
	
	// fetch post parameters sent though HEADER: Content-Type:Application/Json
	$_POST = json_decode ( file_get_contents ( 'php://input' ), true );
	
	// Send error message in case of invalid json request
	if (is_null ( $_POST )) {
		$responseWithHeader = $response->withHeader ( 'Content-type', 'application/json' )->withStatus ( 400 );
		$body = $responseWithHeader->getBody ();
		$exceptionResponse = new ExceptionResponse ();
		$exceptionResponse->errorCode = "ER-6100";
		$exceptionResponse->message = ExceptionCodes::INVALID_JSON;
		
		$body->write ( json_encode ( $exceptionResponse, true ) );
		return $responseWithHeader;
	}
	
	// Read merchant Key from properties file
	$ini_array = parse_ini_file ( __DIR__ . "/resources/properties.ini" );
	
	// Calculate Checksum for the request
	$checksum = ChecksumUtil::generateChecksumForJson ( $_POST, $ini_array ['merchantKey'] );
	
	$_POST ['checksum'] = $checksum;
	
	// Return response filled with request checksum
	$responseWithHeader = $response->withHeader ( 'Content-type', 'application/json' );
	$body = $responseWithHeader->getBody ();
	$body->write ( json_encode ( $_POST, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) );
	
	return $responseWithHeader;
}
