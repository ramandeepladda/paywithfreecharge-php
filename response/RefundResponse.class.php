<?php
require_once __DIR__ . '/AbstractResponse.class.php';
class RefundResponse extends AbstractResponse {
	var $refundTxnId;
	var $refundMerchantTxnId;
	var $merchantTxnId;
	var $refundedAmount;
	var $status;
	
}