<?php
require_once __DIR__ . '/AbstractResponse.class.php';
class GetTransactionStatusResponse extends AbstractResponse {
	var $txnId;
	var $merchantTxnId;
	var $amount;
	var $mode;
	var $status;
}