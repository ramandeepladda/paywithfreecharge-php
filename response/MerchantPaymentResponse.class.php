<?php
require_once __DIR__ . '/AbstractResponse.class.php';
class MerchantPaymentResponse extends AbstractResponse {
	var $txnId;
	var $status;
	var $merchantTxnId;
	var $amount;
	var $pgUsed;
	var $pgUnderlier;
	var $metadata;
	var $errorCode;
	var $errorMessage;
}